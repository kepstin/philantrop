# Copyright 2011 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="Binary diff/patch utility"
DESCRIPTION="
bsdiff and bspatch are tools for building and applying patches to binary files.
By using suffix sorting (specifically, Larsson and Sadakane's qsufsort) and taking
advantage of how executable files change, bsdiff routinely produces binary patches
siginificantly smaller than those produced by similar tools.
"
HOMEPAGE="https://www.daemonology.net/${PN}/"
#DOWNLOADS="${HOMEPAGE}${PNV}.tar.gz"
DOWNLOADS="https://ftp.osuosl.org/pub/gentoo/distfiles/bsdiff-4.3.tar.gz"
LICENCES="BSD-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        app-arch/bzip2
"

src_compile() {
    edo ${CC} ${CFLAGS} ${LDFLAGS} -o bsdiff bsdiff.c -lbz2
    edo ${CC} ${CFLAGS} ${LDFLAGS} -o bspatch bspatch.c -lbz2
}

src_test() {
    edo ./bsdiff "${FILES}"/test "${FILES}"/test2 "${TEMP}"/test.bsdiff
    edo cmp "${FILES}"/test.bsdiff "${TEMP}"/test.bsdiff
    edo ./bspatch "${FILES}"/test "${TEMP}"/test2 "${TEMP}"/test.bsdiff
    edo cmp "${FILES}"/test2 "${TEMP}"/test2
}

src_install() {
    dobin bs{diff,patch}
    doman bs{diff,patch}.1
}

